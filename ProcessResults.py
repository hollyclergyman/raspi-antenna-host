#!/usr/bin/env python3

import paramiko
import platform
import getpass
import pandas as pd
import numpy as np
import os
from scipy import fftpack
from matplotlib import pyplot as plt


class ProcessResults():
    
    def __init__(self):
        host = 'raspberrypi'
        uname = 'pi'
        ssh = paramiko.SSHClient()
        #hkeys = paramiko.
    
    def request_values(self, a, f):
        host = 'raspberrypi'
        uname = 'pi'
        ssh = paramiko.SSHClient()
        sysuser = getpass.getuser()
        '''
        please refer to:
        https://www.w3resource.com/python-exercises/python-basic-exercise-54.php
        '''
        if 'Linux' in platform.system():
            '''
            please refer to:
            https://stackoverflow.com/questions/1854/python-what-os-am-i-running-on
            '''
            keys = '/home/' + sysuser + '/.ssh/external'
            
        #if 'windows' in platform.system():
        #    keys = "C:\users\ " + sysusers + "\application_data\ssh" 
        try:
            ssh.load_system_host_keys()
            # avoids erros concerning the known_hosts file in the .ssh directory
            # solution from:
            # https://github.com/onyxfish/relay/issues/11
            ssh.connect(
                host, 
                username=uname, 
                key_filename=keys
            )
            '''
            please refer to:
            http://docs.paramiko.org/en/2.4/api/client.html
            '''
        except OSError:
            '''
            this exception replaces the old exception for a failed socket connection:
            https://docs.python.org/3.6/library/exceptions.html#OSError
            '''
            return None
        except paramiko.BadHostKeyException:
            return None
        
        tr = ssh.get_transport()
        # opens a usable connection interface between client and server
        channel = tr.open_session()
        # opens a channel, which allows to interchange information between both 
        # connected systems
        channel.exec_command(
            "python3 StartFile.py -a {} -f {}".format(np.int(a), np.int(f))
        )
        # executes the necessary command on the server
        s = channel.recv_exit_status()
        # checks for the status of the command and functions as a lock, which 
        # halts the function, until the server has responded
        # please refer to:
        # https://stackoverflow.com/questions/29994615/how-do-i-know-the-remote-process-is-running-or-complete#29995089
        if s == 0:
            _ls_in, _ls_out, _ls_error = ssh.exec_command("ls Agrartechnik/Results")
            l = _ls_out.readlines()
            rl = [r.strip("\n").split(" ") for r in l]
            d = []
            for x in rl:
                try:
                    dv = np.datetime64(x[0])
                except ValueError:
                    continue
                d.append(dv)
            f = " ".join(rl[d.index(max(d))])

            sftp = ssh.open_sftp()
            raspi_path = "Agrartechnik/Results/{}".format(f)
            host_path = "../Results/{}".format(f)
            sftp.get(raspi_path, host_path)
            # please refer the documentation to the sftp server to:
            # https://medium.com/@keagileageek/paramiko-how-to-ssh-and-file-transfers-with-python-75766179de73 and
            # https://stackoverflow.com/questions/8669165/paramiko-sftp-get
            try:
                sftp.remove(raspi_path)
                # please refer to:
                # http://docs.paramiko.org/en/2.4/api/sftp.html#paramiko.sftp_client.SFTPClient.remove
            except IOError:
                print("Error deleting file")
            sftp.close()
            ssh.close()
            return None
        else:
            print("Error executing command on external server")
            return None
            # this condition terminates the script in case the command on the raspberrypi failed
    
    def read_csv_file(self):
        try:
            dl = [f for f in os.listdir("../Results/") if ".~lock" not in f]
            #  creates a list of filenames, which exist in the given directory
        except FileNotFoundError:
            return None
        l = []
        for f in dl:
            e = f.split('/')
            l.append(np.datetime64(e[-1].split()[0]))
            # creates a list of datetime objects with the timestamps of the files found
        i = "../Results/" + dl[l.index(max(l))]
        # selects the file with the latest datetime stamp and adds the relative path to the name
        f = pd.read_csv(i, sep=",", header=None)
        f = f[1]
        # this step drops the counter column, which is read from the csv file and is not needed in the 
        # ongoing processing
        
        f.str.replace("i", "j").apply(lambda x: np.complex128(x))
        # converts the object type values in the dataframe to np.complex
        # values, thus representing the results of the inert fft
        # solution from:
        # https://stackoverflow.com/questions/18919699/python-pandas-complex-number#18919965
        
        #os.remove(dl[i])
        # deletes the file after being transfered to a dataframe
        return f
    
    def compute_fft(self, cfreq=1090e6, amount=1):
        #rec = self.request_values(a=amount, f=cfreq)
        _in = self.read_csv_file()
        l = []
        for v in _in.index:
            l.append(np.angle(_in.loc[v], deg=False))
        n = pd.Series(data=l)
        return n
        #fft_res = _in.rolling(np.int(cfreq/4)).apply(
        #    lambda x: (1/cfreq) * abs(fftpack.fft(x, cfreq))
        #    )
        """
        Please refer to the explanation from University of Kaiserslautern and 
        at the scipy page:
        https://kluedo.ub.uni-kl.de/frontdoor/deliver/index/docId/4293/file/exact_fft_measurements.pdf
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.fftpack.fft.html#scipy.fftpack.fft

        The calculation is done as a window function to 
        eliminate errors in the amplitude of the measurement.
        Therefore, the function uses the rolling function of the pandas package
        to apply a conversion function to the samples read from the antenna.
        As the size of the window matches the aimed center frequency, no further conversion is 
        executed. Otherwise, the required window size would be sample rate divided by 2, which is effectively 
        the center frequency times 2 and divided by 2 .
        """
        #return fft_res

    def signal_strength(self, base_power, cfreq=1090e6, amount=5):
        _in = self.compute_fft(
            cfreq=cfreq, 
            amount=amount
        )
        r = _in.apply(lambda x: 0.1 * np.log10(x) / base_power)
        mean = np.around(r.mean(), 2)
        std = np.around(r.std(), 2)
        return [mean, std]

    def calc_antenna_gain(self):
        pass

    '''
    Program also work as standalone variant to allow operation from single computer
    Should allow coordinate definition as interactive input

    As RFID works with a bandwith around the aim frequency, the measurement 
    should aim at the bandwith of ~40kHz around the aim frequency
    All other frequencies are dismissed
    Measurement of strength for discrete number of frequencies in 
    measured range
    '''
    
#ProcessResults().request_values(1, 1090e6)
#ProcessResults().read_csv_file()
ProcessResults().compute_fft()

